Pullus Video Monitor 29.11.2022
===============================

The Pullus Video Monitor is a simple MacOS camera (video input) application.

It uses one window to select a camera (video device) and to display its stream.

![Main Widnow](./Documentation/Window.png)

The application was created to understand the basics of the "AVFoundation" framework with Swift.


Requirements
------------

Developed and tested with MacOS 10.12 and Xcode 9.2.

This software was intentionally developed for macOS 10.12 to be able to continue using older systems.
