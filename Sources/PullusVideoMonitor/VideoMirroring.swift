import AVFoundation

///
/// A enumeration to bundle `automaticallyAdjustsVideoMirroring` and
/// `isVideoMirrored` of an `AVCaptureConnection` in a single value.
///
enum VideoMirroring {
	
	case auto
	case on
	case off
	
	@discardableResult
	func apply(to aConnection: AVCaptureConnection) -> Bool {
		switch self {
		case .auto:
			aConnection.automaticallyAdjustsVideoMirroring = true
			
			return true
			
		case .on:
			guard aConnection.isVideoMirroringSupported else { return false }
			
			aConnection.automaticallyAdjustsVideoMirroring = false
			aConnection.isVideoMirrored = true
			
			return true
			
		case .off:
			aConnection.automaticallyAdjustsVideoMirroring = false
			aConnection.isVideoMirrored = false
			
			return true
		}
	}
	
}
