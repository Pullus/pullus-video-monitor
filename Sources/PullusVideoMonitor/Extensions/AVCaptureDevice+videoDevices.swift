import AVFoundation

extension AVCaptureDevice {
	
	static func videoDevices() -> [AVCaptureDevice] {
		return AVCaptureDevice.devices().filter { $0.hasMediaType(.video) }
	}
	
}
