import Swift

extension Array {
	
	func at(_ index: Index) -> Element? {
		guard indices.contains(index) else { return nil }
		
		return self[index]
	}

}
