import AVFoundation

extension AVCaptureDevice {
	
	///
	/// The localized name and the manufacturer.
	///
	var localizedInfo: String {
		return String(format: "%@ (%@)", localizedName, manufacturer)
	}
	
}
