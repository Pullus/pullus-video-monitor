import AVFoundation

extension AVCaptureSession {
	
	func addCheckedInput(_ input: AVCaptureInput) -> Bool {
		guard canAddInput(input) else { return false }
		
		addInput(input)
	
		return true
	}
	
}
