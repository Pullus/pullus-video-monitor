import AVFoundation

extension AVCaptureSession {
	
	func addCheckedOutput(_ output: AVCaptureOutput) -> Bool {
		guard canAddOutput(output) else { return false }
		
		addOutput(output)
		
		return true
	}
	
}
