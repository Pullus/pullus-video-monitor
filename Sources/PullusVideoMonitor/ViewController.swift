import AVFoundation
import Cocoa

///
/// The view controller of the main view.
///
/// It coordinates a `VideoViewController` with a subview to display the video
/// stream and a `NSPopUpButton` to select a camera/video stream.
///
class ViewController : NSViewController {
	
	///
	/// The main controller to handle an `AVCaptureSession` and to display the
	/// video stream.
	///
	private
	var videoViewController: VideoViewController?
	
	// MARK: - NSView
	
	///
	/// The subview to display the video stream.
	///
	@IBOutlet weak
	var videoView: NSView!
	
	///
	/// A pop up button to select the camera/video device.
	///
	@IBOutlet weak
	var deviceSelector: NSPopUpButtonCell!
	
	@IBAction
	func selectDevice(_ sender: NSPopUpButtonCell) {
		selectDevice(at: deviceSelector.indexOfSelectedItem)
	}
	
	///
	/// Instructs the video view controller to change the video device.
	///
	func selectDevice(at anIndex: Int) {
		guard let device = devices.at(anIndex) else { return }
		
		videoViewController?.select(videoDevice: device)
	}
	
	@IBAction
	func close(_ sender: NSButton) {
		view.window?.close()
	}
	
	// MARK: - NSViewController
	
	///
	/// A list of video devices.
	///
	/// `didSet` notifies the video view controller to use the first not
	/// suspended video device.
	///
	private
	var devices: [AVCaptureDevice] = [] {
		didSet {
			deviceSelector.removeAllItems()
			deviceSelector.addItems(withTitles: devices.map { $0.localizedInfo } )
			
			// Select first not suspended device
			if let index = devices.index(where: { !$0.isSuspended } ) {
				selectDevice(at: index)
				deviceSelector.selectItem(at: index)
			}
		}
	}
	
	///
	/// Initializes the video view controller, fetches all available video
	/// devices and selects the first non-suspended device to display.
	///
	override
	func viewDidLoad() {
		super.viewDidLoad()
		
		videoViewController = VideoViewController(controlLayerOf: videoView, gravity: .resizeAspect)

		// Set the background color of the layer, that was added by the
		// VideoViewController.
		// The background is visible, if a video devices is selected but
		// suspended.
		videoView.layer?.backgroundColor = NSColor.lightGray.cgColor
		
		// Fetch available video devices
		devices = AVCaptureDevice.videoDevices()
	}
	
	override
	func viewDidAppear() {
		super.viewDidAppear()
		
		videoViewController?.start()
	}
	
	override
	func viewDidDisappear() {
		super.viewDidDisappear()
		
		videoViewController?.stop()
	}
	
}
