import AVFoundation
import Cocoa

///
/// Connects a camera/video device to view.
///
/// Creates an `AVCaptureSession` with an input, an output and a
/// `AVCaptureVideoPreviewLayer` that is embedded into a `NSView`.
///
class VideoViewController {
	
	///
	/// The session that connects all components.
	///
	private
	let session: AVCaptureSession
	
	///
	/// The layer that displays the video stream.
	///
	private
	let previewLayer: AVCaptureVideoPreviewLayer
	
	// MARK: Live Cycle
	
	///
	/// Creates a session and preview view and embeds them in the specified
	/// view.
	///
	init?(controlLayerOf view: NSView, gravity: AVLayerVideoGravity = .resizeAspectFill) {
		// Create the session
		session = AVCaptureSession()
		session.sessionPreset = AVCaptureSession.Preset.photo

		// Create and attach the preview layer to the session
		previewLayer = AVCaptureVideoPreviewLayer(session: session)
		previewLayer.videoGravity = gravity

		guard session.addCheckedOutput(AVCaptureVideoDataOutput()) else { return nil }

		// Attach the previewlayer to the view
		view.layer = previewLayer
		view.wantsLayer = true
	}
	
	// MARK: - Control

	///
	/// Connects the video device as an input device with the session.
	///
	@discardableResult
	public
	func select(videoDevice: AVCaptureDevice, mirroring: VideoMirroring = .on) -> Bool {
		// Create and attach a device input to the session
		session.inputs.forEach { session.removeInput($0) }
		guard let input = try? AVCaptureDeviceInput(device: videoDevice) else { return false }
		guard session.addCheckedInput(input) else { return false}
		
		// Update mirroring of connection
		guard let connection = previewLayer.connection else { return false}
		mirroring.apply(to: connection)
		
		return true
	}
	
	private
	let sessionQueue: DispatchQueue = DispatchQueue(label: "Video View Controller queue")
	
	func start() {
		if !session.isRunning {
			sessionQueue.async { self.session.startRunning() }
		}
	}
	
	func stop() {
		if session.isRunning {
			sessionQueue.async { self.session.stopRunning() }
		}
	}
	
}
